<?php

namespace Code20\DepotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    private $session;
    
    public function indexAction($name)
    {
        return $this->render('Code20DepotBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function birthdaysAction($name, $year, $month, $day)
    {
        $birthdays = array();
        for($i = $year; $i <= 2014; $i++)
        {
            $birthday = new \DateTime();
            $birthday->setDate($i, $month, $day);
            $birthdays[] = $birthday;
        }
        
        $datetest = new \DateTime();
        $datetest->setDate(1960, 1, 1);
        
        return $this->render('Code20DepotBundle:Default:birthdays.html.twig', array(
            'birthdays' => $birthdays
        ));
    }
    
    public function sessionSetAction($value)
    {
        $session = $this->get('session');
        $session->set('NASZA_WARTOSC', $value);
        
        return $this->redirect($this->generateUrl('get_my_own_value'));
    }
    
    public function sessionGetAction()
    {
        $session = $this->get('session');
        
        $naszaWartosc = $session->get('NASZA_WARTOSC');
        
        return new Response("Ilosc kasy na moim koncie: " . $naszaWartosc . "$");
    }
}
