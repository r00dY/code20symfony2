<?php

namespace Code20\DepotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NumbersController extends Controller
{
    public function numbersAction($divider)
    {
        return $this->render('Code20DepotBundle:Numbers:numbers.html.twig', array('divider' => $divider));
    }
    
    public function aboutUsAction()
    {
        return $this->render('Code20DepotBundle:Numbers:aboutus.html.twig', array());
    }
    
}
