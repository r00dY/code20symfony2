<?php

namespace Code20\DepotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Code20\DepotBundle\Entity\Person;
use Code20\DepotBundle\Entity\Animal;


class DoctrineController extends Controller
{
//    public function addPersonAction($name, $surname, $age)
//    {
//        $person = new Person();
//        $person->setName($name);
//        $person->setSurname($surname);
//        $person->setAge($age);
//        
//        $entityManager = $this->getDoctrine()->getManager();
//        
//        $entityManager->persist($person);
//        $entityManager->flush();
//        
//        return new Response("OK");
//    }
    
    public function addPersonAction(Request $request)
    {
        $person = new Person();
        
        $form = $this->createFormBuilder($person)
                ->add('name', 'text')
                ->add('surname', 'text')
                ->add('age', 'integer')
                ->add('save', 'submit', array('label' => 'Add person'))
                ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($person);
            $entityManager->flush();
            
            return $this->redirect($this->generateUrl('show_people'));
        }
        
        return $this->render('Code20DepotBundle:Doctrine:addPerson.html.twig', array(
            'formularz' => $form->createView()
        ));
    }
    
    public function showPeopleAction()
    {
        $repository = $this->getDoctrine()->getRepository('Code20DepotBundle:Person');
        
        $people = $repository->findAll();
        
        return $this->render('Code20DepotBundle:Doctrine:people.html.twig', array('ludzie' => $people));
    }
    
    public function newAgeAction(Request $request, $personId, $newAge)
    {
        $repository = $this->getDoctrine()->getRepository('Code20DepotBundle:Person');
        
        $person = $repository->find($personId);
        $person->setAge($newAge);
        
        $this->getDoctrine()->getManager()->flush();
        
        return new Response("newAge OK");
    }
    
    public function addAnimalToPersonAction($nickname, $personId)
    {
        $animal = new Animal();
        $animal->setNickname($nickname);
        
        $person = $this->getDoctrine()->getRepository('Code20DepotBundle:Person')->find($personId);
        
        $animal->setPerson($person);
        
        $this->getDoctrine()->getManager()->persist($animal);
        $this->getDoctrine()->getManager()->flush();
        
        return new Response("animal added");
    }
}
